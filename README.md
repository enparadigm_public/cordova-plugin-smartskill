

# Smartskill Cordova Plugin

## Installation
```sh
$ cd CORDOVA_PROJECT
$ cordova plugin add https://gitlab.com/enparadigm_public/cordova-plugin-smartskill.git
```
**Note:** The plugin which we are using is present in private repository. So you need to add following lines in your gradle.properties file.
```
artifactory_repo_url=ARTIFACTORY_URL_GIVEN_BY_US
artifactory_username=USERNAME_GIVEN_BY_US
artifactory_password=PASSWORD_GIVEN_BY_US
```


### Handling Push-Notifiction
Our Library can handle the push notification for you. To make it happen we need to add following in your ``onMessageReceived``  and ``onNewToken`` functions of ``FirebaseMessagingService``
```kotlin
@Override
public void onMessageReceived(RemoteMessage remoteMessage) {
    super.onMessageReceived(remoteMessage);
    SmartSkill.handleFcmNotification(getApplicationContext(), remoteMessage);
}

@Override
public void onNewToken(String s) {
    super.onNewToken(s);
    SmartSkill.saveFcmToken(s);
}
```
Also make sure to initialize our library in your Application class. If we are not calling init in your Application class, then the app will crash on receving push Notification.
```kotlin
// Import these packages  
import com.smartskill.plugin.SmartSkillPluginConfig;
import com.enparadigm.init.SmartSkill;
import com.enparadigm.init.SmartSkillConfig;

@Override  
public void onCreate() {  
  //Add this code in your Application class
  //-------START----------  
  SmartSkillPluginConfig pluginConfig = SmartSkillPluginConfig.getInstance(this);
  if (pluginConfig.base_url != null && pluginConfig.base_url.length() > 10) {
      SmartSkillConfig config = new SmartSkillConfig.Builder(pluginConfig.base_url)
              .build();
      SmartSkill.init(this, config);
  }
  //-------END-----------
  super.onCreate();  
}
```

## Plugin functions
There are 4 functions in the plugin,

 1. **isUserInitialised** - To verify SDK has enough information to start.
 2. **start** - To start our SDK
 2. **initialiseAndStart** - To Initialise our SDK with url and other information as well as start's the SDK
 3. **updateFCM** - To update the FCM token


### Sample code to call plugin function
```javascript
function initialise() {
    var smartSkillPlugin = new SmartSkillPlugin();
    var baseUrl = "URL_GIVEN_BY_US";
    var accessToken = "ACCESS_TOKEN";
    var refreshToken = "REFRESH_TOKEN";
    var uniqueID = "DISTINCT_ID";
    var name = "USER_NAME";
    var languageShortForm = "en"; // Language in which SDK need to start.
    var json = '{"location" : "bng"}';// Pass the data which changes frequently about the user. If you don't have any then pass empty string.
  smartSkillPlugin.initialiseAndStart(baseUrl,accessToken,refreshToken,name,languageShortForm,uniqueID,json, function (msg) {
            console.log("msg : " + msg);
        },
        function (err) {
            console.log("error : " + err);
        }
    );
}

function isUserInitialised() {
    var smartSkillPlugin = new SmartSkillPlugin();
    var json = '{"location" : "bng"}'; // Pass the data which changes frequently about the user. If you don't have any then pass empty string.
    smartSkillPlugin.isUserInitialised(json, function (msg) {
            console.log("msg : " + msg);
            // On success you can start the SDK using "start" function
        },
        function (err) {
            console.log("error : " + err);
        }
    );
}

// Make sure to call this function after calling "isUserInitialised". becase this function won't do the initialisation and validation. Due to that you get error when you call it.
function start() {
    var smartSkillPlugin = new SmartSkillPlugin();
    smartSkillPlugin.start(function (msg) {
              console.log("msg : " + msg);
        },
        function (err) {
            console.log("error : " + err);
        }
    );
}

function updateFCM() {
    var smartSkillPlugin = new SmartSkillPlugin();
    var FCMToken = "FIREBASE_MESSAGING_TOKEN";
    smartSkillPlugin.updateFCM(FCMToken,function (msg) {
              console.log("msg : " + msg);
        },
        function (err) {
            console.log("error : " + err);
        }
    );
}

```