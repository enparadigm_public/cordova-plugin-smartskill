var exec = cordova.require('cordova/exec');
var PLUGIN_NAME = "SmartSkillPlugin";
var SmartSkillPlugin = function() {
    console.log('SmartSkillPlugin instanced');
};

SmartSkillPlugin.prototype.isUserInitialised = function(json,onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    var argument = [json];
    exec(successCallback, errorCallback, PLUGIN_NAME, 'isUserInitialised', argument);
};

SmartSkillPlugin.prototype.initialiseAndStart = function(baseUrl,accessToken,refreshToken,name,languageShortForm,uniqueID,json,onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    var argument = [baseUrl,accessToken,refreshToken,name,languageShortForm,uniqueID,json];
    exec(successCallback, errorCallback, PLUGIN_NAME, 'initialiseAndStart', argument);
};

SmartSkillPlugin.prototype.updateFCM = function(FCMToken,onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    var argument = [FCMToken];
    exec(successCallback, errorCallback, PLUGIN_NAME, 'updateFCM', argument);
};

SmartSkillPlugin.prototype.start = function(onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, PLUGIN_NAME, 'start', []);
};

if (typeof module != 'undefined' && module.exports) {
    module.exports = SmartSkillPlugin;
}