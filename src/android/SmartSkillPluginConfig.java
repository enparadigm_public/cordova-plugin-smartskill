package com.smartskill.plugin;

import android.content.Context;
import android.content.SharedPreferences;

public class SmartSkillPluginConfig {

    public String base_url, accessToken, refreshToken, uniqueId, name, json;
    private static SharedPreferences preferences;
    private static SmartSkillPluginConfig instance;

    private static String PREFERENCE_BASE_URL = "BASE_URL";
    private static String PREFERENCE_ACCESS_TOKEN = "ACCESS_TOKEN";
    private static String PREFERENCE_REFRESH_TOKEN = "REFRESH_TOKEN";
    private static String PREFERENCE_JSON = "JSON";
    private static String PREFERENCE_UNIQUE_ID = "UniqueID";
    private static String PREFERENCE_NAME = "NAME";
    private static String PREFERENCE_LANGUAGE_SHORT_FORM = "LANGUAGE_SHORT_FORM";

    private SmartSkillPluginConfig(Context ctx) {
        base_url = getBaseURL(ctx);
        accessToken = getAccessToken(ctx);
        refreshToken = getRefreshToken(ctx);
        uniqueId = getUniqueId(ctx);
        name = getName(ctx);
        json = getJson(ctx);
    }


    public static SmartSkillPluginConfig getInstance(Context ctx) {
        if (instance == null || instance.base_url.length() < 5) {
            instance = new SmartSkillPluginConfig(ctx);
        }
        return instance;
    }

    static SharedPreferences getPreference(Context context) {
        if (preferences == null)
            preferences = context.getSharedPreferences("SmartSkillPluginPreference", Context.MODE_PRIVATE);
        return preferences;
    }

    static String getBaseURL(Context context) {
        return getPreference(context).getString(PREFERENCE_BASE_URL, "");
    }

    static void setBaseURL(Context context, String baseUrl) {
        getPreference(context).edit().putString(PREFERENCE_BASE_URL, baseUrl).apply();
    }

    static String getAccessToken(Context context) {
        return getPreference(context).getString(PREFERENCE_ACCESS_TOKEN, "");
    }

    static void setAccessToken(Context context, String accessToken) {
        getPreference(context).edit().putString(PREFERENCE_ACCESS_TOKEN, accessToken).apply();
    }

    static String getRefreshToken(Context context) {
        return getPreference(context).getString(PREFERENCE_REFRESH_TOKEN, "");
    }

    static void setRefreshToken(Context context, String refreshToken) {
        getPreference(context).edit().putString(PREFERENCE_REFRESH_TOKEN, refreshToken).apply();
    }

    static String getJson(Context context) {
        return getPreference(context).getString(PREFERENCE_JSON, "");
    }

    static void setJson(Context context, String json) {
        getPreference(context).edit().putString(PREFERENCE_JSON, json).apply();
    }

    static String getUniqueId(Context context) {
        return getPreference(context).getString(PREFERENCE_UNIQUE_ID, "");
    }

    static void setUniqueId(Context context, String uniqueId) {
        getPreference(context).edit().putString(PREFERENCE_UNIQUE_ID, uniqueId).apply();
    }

    static String getName(Context context) {
        return getPreference(context).getString(PREFERENCE_NAME, "");
    }

    static void setName(Context context, String name) {
        getPreference(context).edit().putString(PREFERENCE_NAME, name).apply();
    }

    static String getLanguageShortForm(Context context) {
        return getPreference(context).getString(PREFERENCE_LANGUAGE_SHORT_FORM, "en");
    }

    static void setLanguageShortForm(Context context, String languageShortForm) {
        getPreference(context).edit().putString(PREFERENCE_LANGUAGE_SHORT_FORM, languageShortForm).apply();
    }


}