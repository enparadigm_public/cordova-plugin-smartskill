package com.smartskill.plugin;

import android.app.Application;
import android.content.Context;

import com.enparadigm.init.GetLanguageCallback;
import com.enparadigm.init.SmartSkill;
import com.enparadigm.init.SmartSkillConfig;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;

import org.json.JSONArray;
import org.json.JSONException;

public class SmartSkillPlugin extends CordovaPlugin {

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        Context context = cordova.getActivity().getApplicationContext();
        if (action.equalsIgnoreCase("initialiseAndStart")) {
            initialiseAndStart(args, context, callbackContext);
            return true;
        } else if (action.equalsIgnoreCase("isUserInitialised")) {
            isUserInitialised(args, context, callbackContext);
            return true;
        } else if (action.equalsIgnoreCase("updateFCM")) {
            updateFCM(args, callbackContext);
            return true;
        } else if (action.equalsIgnoreCase("start")) {
            start(callbackContext);
            return true;
        }
        return false;
    }

    private void initialiseAndStart(JSONArray arg, Context context, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {

                String baseUrl = arg.getString(0);
                String accessToken = arg.getString(1);
                String refreshToken = arg.getString(2);
                String name = arg.getString(3);
                String languageShortForm = arg.getString(4);
                String uniqueId = arg.getString(5);
                String json = arg.getString(6);

                SmartSkillPluginConfig.setBaseURL(context, baseUrl);
                SmartSkillPluginConfig.setAccessToken(context, accessToken);
                SmartSkillPluginConfig.setRefreshToken(context, refreshToken);
                SmartSkillPluginConfig.setName(context, name);
                SmartSkillPluginConfig.setUniqueId(context, uniqueId);
                SmartSkillPluginConfig.setJson(context, json);
                SmartSkillPluginConfig.setLanguageShortForm(context, languageShortForm);

                SmartSkillConfig config = new SmartSkillConfig.Builder(baseUrl)
                        .build();
                cordova.getActivity().runOnUiThread(() -> {
                    try {

                        try {
                            SmartSkill.init((Application) context.getApplicationContext(), config);
                        } catch (Exception e) {
                            if (!e.getMessage().equalsIgnoreCase("Koin is already started. Run startKoin only once or use loadKoinModules")) {
                                callbackContext.error("Exception : " + e.getMessage());
                                return;
                            }
                        }
                        if (!SmartSkill.canLaunchSDK(json)) {
                            SmartSkill.setPluginData(accessToken, refreshToken, name, languageShortForm, uniqueId, json, new GetLanguageCallback() {
                                @Override
                                public void onSuccess() {
                                    SmartSkill.start(cordova.getActivity());
                                    callbackContext.success("Success");
                                }

                                @Override
                                public void onInvalidLanguage() {
                                    SmartSkill.start(cordova.getActivity());
                                    callbackContext.error("Invalid Language");
                                }

                                @Override
                                public void onFailure(String s) {
                                    callbackContext.error("Error : " + s);
                                }
                            });
                        }
                    } catch (Exception e) {
                        if (e.getMessage().equalsIgnoreCase("Koin is already started. Run startKoin only once or use loadKoinModules")) {
                            SmartSkill.start(cordova.getActivity());
                            callbackContext.success("Success");
                        } else {
                            callbackContext.error("Exception : " + e.getMessage());
                        }
                    }
                });
            } catch (Exception e) {
                callbackContext.error("Exception : " + e.getMessage());
            }
        });
    }

    private void start(CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {

                SmartSkill.start(cordova.getActivity());
                callbackContext.success("Success");
            } catch (Exception e) {
                if (e.getMessage().equalsIgnoreCase("SmartSkill skd has not been initialized")) {
                    callbackContext.error("Please initialize the SDK before start.");
                } else {
                    callbackContext.error("Exception : " + e.getMessage());
                }
            }
        });
    }

    private void isUserInitialised(JSONArray arg, Context context, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            String errorMessage = "";
            try {
                String json = arg.getString(0);
                SmartSkillPluginConfig pluginConfig = SmartSkillPluginConfig.getInstance(context);
                if (pluginConfig.base_url != null && pluginConfig.base_url.length() > 5) {
                    SmartSkillConfig config = new SmartSkillConfig.Builder(pluginConfig.base_url)
                            .build();
                    try {
                        SmartSkill.init((Application) context.getApplicationContext(), config);
                    } catch (Exception e) {
                        if (!e.getMessage().equalsIgnoreCase("Koin is already started. Run startKoin only once or use loadKoinModules")) {
                            errorMessage = e.getMessage();
                        }
                    }

                    if (errorMessage.length() == 0) {
                        if (SmartSkill.canLaunchSDK(json)) {
                            SmartSkill.start(cordova.getActivity());
                            callbackContext.success("Success");
                        } else {
                            callbackContext.error("Failed : User details are mismatching. Please initialise the SDK with new user information");
                        }
                    } else {
                        callbackContext.error("Failed : " + errorMessage);
                    }
                } else {
                    callbackContext.error("Failed : Please initialise the SDK once");
                }
            } catch (Exception e) {
                if (e.getMessage().equalsIgnoreCase("Koin is already started. Run startKoin only once or use loadKoinModules")) {
                    callbackContext.success("Success");
                } else {
                    callbackContext.success("Failed");
                }
            }
        });
    }


    private void updateFCM(JSONArray arg, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String fcmToken = arg.getString(0);
                SmartSkill.saveFcmToken(fcmToken);
                callbackContext.success("Success");
            } catch (Exception e) {
                callbackContext.success("Failed");
            }
        });
    }

//    private void initSmartSkill(String baseUrl, Context context, CallbackContext callbackContext) {
//        cordova.getThreadPool().execute(() -> {
//            try {
//                SmartSkillPluginConfig.setBaseURL(context, baseUrl);
//                SmartSkillConfig config = new SmartSkillConfig.Builder(baseUrl)
//                        .build();
//                SmartSkill.init((Application) context.getApplicationContext(), config);
//                callbackContext.success("Success");
//            } catch (Exception e) {
//                if (e.getMessage().equalsIgnoreCase("Koin is already started. Run startKoin only once or use loadKoinModules")) {
//                    callbackContext.success("Success");
//                } else {
//                    callbackContext.success("Failed");
//                }
//            }
//        });
//    }
//
//    private void validateUser(String mobileNumber, CallbackContext callbackContext) {
//
//        cordova.getThreadPool().execute(() -> SmartSkill.validateUser(mobileNumber, 1, new ValidateUserTask() {
//            @Override
//            public void onSuccess() {
//                callbackContext.success("Success");
//            }
//
//            @Override
//            public void onFailure(String s) {
//                callbackContext.error("Failed");
//            }
//        }));
//    }


}
